#!/usr/bin/env bash

docker run \
    --name log-generator \
    -d \
    -v $PWD/logs/httpd/:/out \
    reyiyo/apache-logs-generator --num 0 --output LOG --prefix /out/httpd --sleep 0.2
