#!/usr/bin/env bash

# Wait for Elasticsearch to start up before doing anything.
until curl -s http://elasticsearch:9200/_cat/health -o /dev/null; do
    echo Waiting for Elasticsearch...
    sleep 1
done

# Load default-template
echo "Loading default template..."
curl -s  -H 'Content-Type: application/json' -XPUT http://@elasticsearch:9200/_template/logstash -d@/templates/default-template.json
curl -s  -H 'Content-Type: application/json' -XPUT http://@elasticsearch:9200/_template/singlenode-default-template -d@/templates/singlenode-default-template.json
